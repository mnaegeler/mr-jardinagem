'use strict';

module.exports = {
  up: async (queryInterface) => {
    const [,ptLang] = await queryInterface.bulkInsert('Languages', [
      {
        id: '8158e21c-6b98-413e-a1db-3610cd69d52c',
        abbr: 'en',
        name: 'English',
        active: false,
      },
      {
        id: '69c6921e-a832-4370-aba1-642045c534d3',
        abbr: 'pt_BR',
        name: 'Português',
        active: true,
      },
    ], { returning: ['id'] })

    const [sysAdmin, defaultUser] = await queryInterface.bulkInsert('Roles', [
      {
        id: 'be45ccd8-3d92-4f59-bc86-79d4026625a8',
        labelKey: 'SYSTEM_ADMINISTRATOR',
        reference: 'SYSTEM_ADMINISTRATOR',
        hasFullAccess: true,
        level: 1,
      },
      {
        id: '37b79ec8-56d2-4e67-892a-e6ad3b79a21a',
        labelKey: 'DEFAULT_USER',
        reference: 'DEFAULT_USER',
        level: 2,
      },
    ], { returning: ['id'] })

    await queryInterface.bulkInsert('Users', [
      {
        id: '2cd40d9b-e4b9-41b2-9dc9-c2e9052f4cfc', // Fixed for development reasons (to not lose login after drop tables)
        name: 'Admin',
        email: 'admin@supernova.test',
        // password: admin123
        passwordHash: '$2a$08$iMgk32kr.Zzf7Y0SYgWXf.9mEcIbO.vsrJkEg.Vp.OzvL6XsMioVa',
        LanguageId: ptLang.id,
        RoleId: sysAdmin.id,
      },

      {
        id: 'f9d1d052-a0a6-436d-84d1-7215633a6f2d',
        name: 'User',
        email: 'user@supernova.test',
        // password: user123
        passwordHash: '$2a$08$sMZzDHrqzWI9FMAVuFlrq.QiGGhPXXKN5euTnbSvhWEkC6pw6pGQm',
        LanguageId: ptLang.id,
        RoleId: defaultUser.id,
      },
    ], {});
  },

  down: (queryInterface) => {
    return queryInterface.bulkDelete('Users', null, { truncate: true });
  }
};
