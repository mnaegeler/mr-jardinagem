module.exports = (sequelize, DataTypes) => {
  const View = sequelize.define("View", {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      defaultValue: sequelize.literal("uuid_generate_v4()"),
    },
    filters: DataTypes.JSON,
    include: DataTypes.JSON,
    key: DataTypes.STRING,
    labelKey: DataTypes.STRING,
    showOnMenu: DataTypes.BOOLEAN,
    slug: DataTypes.STRING,
    type: DataTypes.STRING,
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: false,
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: false,
    },
  });

  View.associate = (models) => {
    View.belongsTo(models.MenuSection);
    View.belongsTo(models.Model);
    View.hasMany(models.ViewAction);
    View.hasMany(models.ViewField);
    View.hasMany(models.ViewPermission);
    View.hasMany(models.ViewRowAction);
    View.belongsTo(models.User, { as: "Owner", constraints: false });
  };

  return View;
};
