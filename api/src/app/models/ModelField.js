module.exports = (sequelize, DataTypes) => {
  const ModelField = sequelize.define("ModelField", {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      defaultValue: sequelize.literal("uuid_generate_v4()"),
    },
    default: DataTypes.STRING,
    length: DataTypes.INTEGER,
    modelFieldType: DataTypes.STRING,
    name: DataTypes.STRING,
    selectOptions: DataTypes.JSON,
    targetModel: DataTypes.STRING,
    targetModelAlias: DataTypes.STRING,
    type: DataTypes.STRING,
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: false,
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: false,
    },
  });

  ModelField.associate = (models) => {
    ModelField.belongsTo(models.Model);
    ModelField.hasMany(models.ViewField);
    ModelField.belongsTo(models.User, { as: "Owner", constraints: false });
  };

  return ModelField;
};
