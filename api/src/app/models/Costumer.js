module.exports = (sequelize, DataTypes) => {
  const Costumer = sequelize.define("Costumer", {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      defaultValue: sequelize.literal("uuid_generate_v4()"),
    },
    address: DataTypes.TEXT,
    cnpj: DataTypes.STRING,
    email: DataTypes.STRING,
    name: DataTypes.STRING,
    phone: DataTypes.STRING,
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: false,
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: false,
    },
  });

  Costumer.associate = (models) => {
    Costumer.belongsTo(models.User, { as: "Owner", constraints: false });
  };

  return Costumer;
};
