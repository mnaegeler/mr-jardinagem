const nodemailer = require('nodemailer')
require('dotenv').config({
  path: process.env.NODE_ENV === 'test' ? '.env.test' : '.env'
});

const { User, UserPasswordToken, Language, Role } = require('../models');

const sessionController = {
  async store (req, res) {
    const { email, password } = req.body;

    let messages = []
    if (!email) {
      messages.push('E-mail not provided')
    }

    if (!password) {
      messages.push('Password not provided')
    }

    if (messages.length) {
      return res.status(401).json({ messages })
    }

    const user = await User.findOne({ where: { email }, include: [ Role, Language ] })

    if (!user) {
      return res.status(401).json({ messages: ['User not found'] })
    }

    if (!user.passwordHash) {
      messages.push(`The user doesn't have a password`)
    }

    if (messages.length) {
      return res.status(401).json({ messages })
    }

    if (!(await user.checkPassword(password))) {
      return res.status(401).json({ messages: ['Incorrect password'] })
    }

    return res.json({
      user,
      token: user.generateToken()
    });
  },

  async sendPasswordResetMail (req, res) {
    const { email } = req.body;

    const user = await User.findOne({ where: { email }, include: [ Language ] })

    if (!user) {
      return res.status(401).json({ messages: ['User not found'] })
    }

    const token = await UserPasswordToken.create({ UserId: user.id })

    const url = process.env.NODE_ENV === 'development' ? 'http://localhost:1234#/set-password' : ''
    const mailMessage = `Para ${user.passwordHash ? 'redefinir' : 'criar'} sua senha, acesse o link abaixo: ${url}/${token.id}`

    let transporter = nodemailer.createTransport({
      host: process.env.MAIL_HOST,
      port: process.env.MAIL_PORT || 587,
      auth: {
        user: process.env.MAIL_USER,
        pass: process.env.MAIL_PASS,
      },
    });
  
    // send mail with defined transport object
    try {
      await transporter.sendMail({
        from: 'Marcelo <dev@facilite.online>',
        to: user.email,
        subject: user.passwordHash ? "Redefinir senha" : 'Criar senha',
        text: mailMessage, // plain text body
        // html: mailMessage, // html body
      });
    } catch (e) {
      return res.status(500).json({ messages: [e.message] })
    }
  
    // let messages = ['Email not found', email]
    return res.status(200).json({ ok: 1 })
  },

  async setPassword (req, res) {
    const { token, password } = req.body

    const userToken = await UserPasswordToken.findByPk(token)

    if (!userToken) {
      return res.status(400).json({ messages: ['Invalid token'] })
    }

    if (!password) {
      return res.status(400).json({ messages: ['Password not provided'] })
    }

    const user = await User.findByPk(userToken.UserId)
    user.update({ password })

    userToken.destroy()

    return res.status(200).json({ message: 'Password updated' })
  }
}

module.exports = sessionController
