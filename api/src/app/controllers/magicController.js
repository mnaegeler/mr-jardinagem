const fs = require('fs').promises
const util = require('util')
const path = require('path')
const nodemailer = require('nodemailer')
const { Op } = require('sequelize')
const { makeValidations } = require('../validation-rules')
const { uploadImages } = require('../utils/file-upload')

let models = require('../models')
const { User, Model, View, ViewField, ViewAction, ViewRowAction } = models
const { exportMetadata } = require('../../generators/export')

const exec = util.promisify(require('child_process').exec);

const modelsPath = path.resolve(__dirname, '..', 'models')

function camelToSnake (str) {
  return str.replace(/[A-Z]/g, (letter, index) => `${index > 0 ? '_' : ''}${letter.toLowerCase()}`)
}

async function getModelMetadata (modelName) {
  return await Model.findOne({
    where: {
      modelName,
    },
    include: ['ModelFields', 'ModelValidations']
  })
}

async function getViewMetadata (role = null) {  
  if (!role) {
    role = {
      id: null,
      hasFullAccess: false,
    }
  }
  
  return await View.findAll({
    attributes: ['id', 'key', 'labelKey', 'type', 'slug', 'showOnMenu', 'filters', 'include'],
    include: [
      {
        model: models['ViewPermission'],
        required: !role.hasFullAccess,
        where: {
          RoleId: {
            [Op.eq]: role.id,
          },
        },
      },

      {
        model: models['Model'],
      },

      {
        model: models['MenuSection'],
      },

      {
        model: models['ViewField'],
        attributes: { exclude: ['createdAt', 'updatedAt', 'ModelFieldId', 'ViewId'] },
        order: [['order', 'ASC']],
        include: [
          {
            model: models['ModelField'],
            attributes: { exclude: ['createdAt', 'updatedAt', 'ModelFieldId', 'ViewId'] },
          },
        ],
      },

      {
        model: models['ViewAction'],
        attributes: ['id', 'type', 'labelKey', 'class', 'slug', 'params', 'order', 'position'],
        order: [['order', 'ASC']],
      },

      {
        model: models['ViewRowAction'],
        attributes: ['id', 'type', 'labelKey', 'class', 'slug', 'params', 'order', 'position'],
        order: [['order', 'ASC']],
      },
    ],
  })
}

async function generateModelFile (modelInstance, body, isCreating=false) {
  const modelFields = body['ModelFields'].filter(f => !['createdAt', 'updatedAt', 'deletedAt', 'OwnerId'].includes(f.name))
  
  // Create default views
  if (isCreating === true) {
    const slugName = camelToSnake(modelInstance.modelName)

    const listView = await View.create({
      ModelId: modelInstance.id,
      key: `list-${slugName}`,
      labelKey: modelInstance.modelName,
      type: 'ListView',
      slug: `/${slugName}`,
      showOnMenu: true,
      filters: null,
      include: null,
    })

    await ViewAction.create({
      ViewId: listView.id,
      labelKey: 'create',
      class: 'is-primary',
      type: 'navigation',
      slug: `/${slugName}_form`,
      order: 1,
      position: 'top right',
    })

    await ViewRowAction.bulkCreate([
      {
        ViewId: listView.id,
        labelKey: 'edit',
        class: 'is-link',
        type: 'navigation',
        slug: `/${slugName}_form/:id:`,
        order: 1,
      },
      {
        ViewId: listView.id,
        labelKey: 'remove',
        class: 'has-text-danger',
        type: 'delete',
        order: 2,
        params: { id: ":id:" },
      }
    ])

    const formView = await View.create({
      ModelId: modelInstance.id,
      key: `form-${slugName}`,
      labelKey: modelInstance.modelName,
      type: 'FormView',
      slug: `/${slugName}_form`,
      showOnMenu: false,
      filters: null,
      include: null,
    })

    await ViewAction.bulkCreate([
      {
        ViewId: formView.id,
        labelKey: 'back',
        class: 'is-link is-light',
        type: 'navigation',
        slug: `/${slugName}`,
        order: 1,
        position: 'top left',
      },
      {
        ViewId: formView.id,
        labelKey: 'save',
        class: 'is-info',
        type: 'submit',
        slug: `/${slugName}`,
        order: 1,
        position: 'form end',
      }
    ])

    const fieldsForm = []
    const fieldsList = []
    modelFields.forEach(async (field, index) => {
      if (index < 4) {
        fieldsList.push({
          ModelFieldId: field.id,
          order: index,
          class: 'is-2',
          labelKey: field.name,
        })
      }

      fieldsForm.push({
        ModelFieldId: field.id,
        order: index,
        class: 'is-2',
        labelKey: field.name,
      })
    })

    const createdAt = body['ModelFields'].find(f => f.name === 'createdAt')
    // const updatedAt = body['ModelFields'].find(f => f.name === 'updatedAt')
    const ownerId = body['ModelFields'].find(f => f.name === 'OwnerId')

    const dateFields = [
      {
        ModelFieldId: createdAt.id,
        order: modelFields.length,
        class: 'is-2',
        canUpdate: false,
        labelKey: 'createdAt',
      },
      // {
      //   ModelFieldId: updatedAt.id,
      //   order: modelFields.length + 1,
      //   class: 'is-2',
      //   canUpdate: false,
      //   labelKey: 'updatedAt',
      // },
    ]

    const ownerFields = [
      {
        ModelFieldId: ownerId.id,
        order: modelFields.length,
        class: 'is-2',
        canUpdate: false,
        labelKey: 'OwnerId',
      },
    ]

    const listFields = [...fieldsList, ...dateFields, ...ownerFields].map(f => {
      let nf = { ...f }
      nf.ViewId = listView.id
      return nf
    })

    const formFields = [...fieldsForm, ...dateFields].map(f => {
      let nf = { ...f }
      nf.ViewId = formView.id
      return nf
    })

    try {
      await ViewField.bulkCreate([...listFields, ...formFields])

      await exportMetadata()
      await exec('node ./src/generators/models.js')
      await exec('node ./src/generators/syncDatabase.js')

      const filePath = path.resolve(modelsPath, `${body.modelName}.js`)

      models[modelInstance.modelName] = models.sequelize['import'](filePath)
      if (models[modelInstance.modelName].associate) {
        models[modelInstance.modelName].associate(models)
      }
    } catch (e) {console.error(e)}
  }
}

const magicController = {
  async getMeta (req, res) {
    const user = await User.findByPk(req.userId, { include: [models['Role'], models['Language']] })
    let languageAbbr = 'en'

    if (user && user.Language && user.Language.active) {
      languageAbbr = user.Language.abbr
    } else if (req.get('Language')) {
      languageAbbr = req.get('Language')
    }

    let labels = null
    let error = ''
    try {
      const jsonPath = path.resolve(__dirname, '../..', 'labels', `${languageAbbr}.json`)
      labels = await fs.readFile(jsonPath, 'utf8')
    } catch (e) {
      error = `Translation file for language ${languageAbbr} not found`
    }

    if (!error) {
      try {
        labels = JSON.parse(labels)
      } catch (e) {
        error = `Translation file for language ${languageAbbr} has syntax errors`
      }
    }

    if (error) {
      console.error(error)
      return res.status(500).send({ messages: [error] })
    }

    try {
      const views = await getViewMetadata(user ? user.Role : null)

      return res.status(200).send({ views, labels, user })
    } catch (e) {
      console.error(e)
      return res.status(500).send(e)
    }
  },

  async browse (req, res) {
    const user = await User.findByPk(req.userId, { include: [ models['Role'] ] })

    const modelName = req.get('Model')
    let page = req.query.page
    let include = req.query.include
    let whereQuery = req.query.where
    let order = req.query.order
    try {
      whereQuery = JSON.parse(whereQuery)['AND']
    } catch (e) {
      whereQuery = null
    }

    if (!include) {
      include = []
    }

    include = include.map(i => {
      try {
        i = JSON.parse(i)
        i.model = models[i.model]
        i.required = false
      } catch (e) {}

      return i
    })

    include.push('Owner')

    if (!modelName || !models[modelName]) {
      return res.status(400).send({ messages: ['Model not provided'] })
    }

    const hasPermission = await models.Model.findOne({
      where: {
        modelName
      },

      include: {
        model: models['ModelPermission'],
        required: !user.Role.hasFullAccess,
        where: {
          action: 'browse',
          RoleId: {
            [Op.eq]: user.Role.id,
          },
        },
      }
    })

    if (!hasPermission) {
      return res.status(403).send({ messages: ['Forbidden'] })
    }
    
    let where = null
    if (whereQuery) {
      where = {}
      whereQuery.forEach(clause => {
        if (clause.value && clause.value !== 'null') {
          if (!where[clause.name]) {
            where[clause.name] = {}
          }

          where[clause.name][Op[`${clause.criteria}`]] = clause.value
        }
      })
    }

    // When searching for roles, it should be able to find only roles with `level` greater than or equal to the current user
    // Lower is higher in the hierarchy
    if (modelName === 'Role') {
      if (!where) {
        where = {}
      }

      where['level'] = { [Op[`gte`]]: user.Role.level }

    } else if (modelName === 'User') {
      // In the case of User model, only greater and equal `level`s should be listed
      if (!where) {
        where = {}
      }

      include.push('Role')
      if (!where['$Role.level$']) {
        where['$Role.level$'] = { [Op[`gte`]]: user.Role.level }
      }
    }

    if (order && order.length) {
      try {
        order[0] = JSON.parse(order[0])
      } catch (e) {}
    } else {
      order = [ ['updatedAt', 'DESC'] ]
    }

    try {
      const limit = 100
      page = page ? Number(page) : 1
      const offset = page === 1 ? 0 : limit * (page - 1)
      const result = await models[modelName].findAll({ include, where, order, limit, offset })

      return res.status(200).send({ result })
    } catch (error) {
      console.log(error)
      return res.status(500).send({ error: error.message })
    }
  },

  async show (req, res) {
    const user = await User.findByPk(req.userId, { include: [ models['Role'], models['Language'] ] })

    const id = req.params.id
    const modelName = req.get('Model')
    let include = req.query.include

    if (!modelName) {
      return res.status(400).send({ messages: ['Model not provided'] })
    }

    const hasPermission = await models.Model.findOne({
      where: {
        modelName
      },

      include: {
        model: models['ModelPermission'],
        required: !user.Role.hasFullAccess,
        where: {
          action: 'show',
          RoleId: {
            [Op.eq]: user.Role.id,
          },
        },
      }
    })

    if (!hasPermission) {
      return res.status(403).send({ messages: ['Forbidden'] })
    }

    if (!include) {
      include = []
    }

    const orderModels = []

    include = include.map(i => {
      try {
        if (typeof i === 'string') {
          i = JSON.parse(i)
        }

        const name = i.model
        i.model = models[name]
        i.required = false

        if (!i.order) {
          i.order = [ 'createdAt' ]
        } else if (i.order === 'order') {
          orderModels.push([ name, 'order' ])
        }
      } catch (e) {}

      return i
    })

    include.push('Owner')
    
    let result = null
    try {
      result = await models[modelName].findByPk(id, { include })
      if (orderModels.length) {
        orderModels.forEach(i => {
          result[`${i[0]}s`] = result[`${i[0]}s`].sort((a, b) => {
            if (a[i[1]] > b[i[1]]) {
              return 1
            }

            if (a[i[1]] < b[i[1]]) {
              return -1
            }

            return 0
          })
        })
      }
    } catch (e) { console.error(e) }

    if (!result) {
      const model = await Model.findOne({ where: { modelName } })

      return res.status(404).send({ messages: [`${model.name} not found`] })
    }

    return res.status(200).send({ result })
  },

  async store (req, res) {
    const user = await User.findByPk(req.userId, { include: [ models['Role'], models['Language'] ] })

    const modelName = req.get('Model')
    const body = req.body

    if (!modelName) {
      return res.status(400).send({ messages: ['Model not provided'] })
    }

    const hasPermission = await models.Model.findOne({
      where: {
        modelName
      },

      include: {
        model: models['ModelPermission'],
        required: !user.Role.hasFullAccess,
        where: {
          action: 'store',
          RoleId: {
            [Op.eq]: user.Role.id,
          },
        },
      }
    })

    if (!hasPermission) {
      return res.status(403).send({ messages: ['Forbidden'] })
    }
    
    const modelMeta = await getModelMetadata(modelName)

    let element = null
    try {
      const userId = req.userId
      body.OwnerId = userId

      try {
        await makeValidations(modelMeta, body, user)
      } catch (e) {
        return res.status(500).send({ messages: [e.message] })
      }

      await uploadImages(modelMeta, body)

      element = await models[modelName].create(body)

      const bodyToCreate = { ...body }

      // Auto-wire the record with the owner (the logged-in user)
      Object.keys(bodyToCreate).forEach(async key => {
        const metaField = modelMeta.ModelFields.find(m => m.name === key)

        // hasMany
        if (Array.isArray(bodyToCreate[key]) === true && metaField.type === 'hasMany') {
          if (modelName === 'Model') {
            bodyToCreate[key].push({ name: 'createdAt', type: 'datetime', modelFieldType: 'DataTypes.DATE' })
            bodyToCreate[key].push({ name: 'updatedAt', type: 'datetime', modelFieldType: 'DataTypes.DATE' })
            bodyToCreate[key].push({ name: 'deletedAt', type: 'datetime', modelFieldType: 'DataTypes.DATE' })
            bodyToCreate[key].push({ name: 'OwnerId', type: 'belongsTo', targetModel: 'User', targetModelAlias: 'Owner' })
          }

          bodyToCreate[key].forEach(async (arrayItem, index) => {
            arrayItem.OwnerId = userId

            if (modelName === 'Question') {
              arrayItem.order = index
            }

            let item = await models[metaField.targetModel].upsert({ ...arrayItem }, { returning: true })
            await element[`add${key}`](item[0])
            bodyToCreate[key][index].id = item[0].id
          })
        }
      })
    } catch (e) {
      console.error(e)
      return res.status(500).send({ messages: [e.message] })
    }

    if (modelName === 'User') {
      const user = await User.findByPk(element.id)
      const token = await models['UserPasswordToken'].create({ UserId: user.id }, { returning: ['id'] })

      const url = `${process.env.APP_URL}#/set-password`
      const mailMessage = `Para ${user.passwordHash ? 'redefinir' : 'criar'} sua senha, acesse o link abaixo: ${url}/${token.id}`

      let transporter = nodemailer.createTransport({
        host: process.env.MAIL_HOST,
        port: process.env.MAIL_PORT || 587,
        auth: {
          user: process.env.MAIL_USER,
          pass: process.env.MAIL_PASS,
        },
      });
    
      // send mail with defined transport object - does not wait the email to be sent
      transporter.sendMail({
        from: 'Marcelo <dev@facilite.online>',
        to: user.email,
        subject: user.passwordHash ? "Redefinir senha" : 'Criar senha',
        text: mailMessage, // plain text body
        // html: mailMessage, // html body
      });
    }

    if (modelName === 'Model') {
      try {
        await generateModelFile(element, body, true)
      } catch (e) {
        return res.status(500).send({ messages: [e.message] })
      }
    }

    return res.status(200).send({ id: element.id })
  },

  async update (req, res) {
    const user = await User.findByPk(req.userId, { include: [ models['Role'], models['Language'] ] })

    const id = req.params.id
    const modelName = req.get('Model')
    const body = req.body

    if (!modelName) {
      return res.status(400).send({ messages: ['Model not provided'] })
    }

    const hasPermission = await models.Model.findOne({
      where: {
        modelName
      },

      include: {
        model: models['ModelPermission'],
        required: !user.Role.hasFullAccess,
        where: {
          action: 'update',
          RoleId: {
            [Op.eq]: user.Role.id,
          },
        },
      }
    })

    if (!hasPermission) {
      return res.status(403).send({ messages: ['Forbidden'] })
    }
    
    let element = null
    try {
      element = await models[modelName].findByPk(id)
    } catch (e) { console.error(e) }

    if (!element) {
      const model = await Model.findOne({ where: { modelName } })

      return res.status(404).send({ messages: [`${model.name} not found`] })
    }

    const userId = req.userId
    const modelMeta = await getModelMetadata(modelName)

    try {
      await makeValidations(modelMeta, body, user, id)
    } catch (e) {
      return res.status(500).send({ messages: [e.message] })
    }

    const bodyToUpdate = { ...body }
    Object.keys(bodyToUpdate).forEach(async key => {
      const metaField = modelMeta.ModelFields.find(m => m.name === key)

      // hasMany
      if (Array.isArray(bodyToUpdate[key]) === true && metaField.type === 'hasMany') {
        let relationsToRemove = await element[`get${key}`]()

        try {
          bodyToUpdate[key] = bodyToUpdate[key].map(async (arrayItem) => {
            if (arrayItem.id) {
              // If the relation still exists, remove from removal list
              const index = relationsToRemove.findIndex(r => r.id === arrayItem.id)
              if (index !== -1) {
                relationsToRemove.splice(index, 1)
              }
            }
            
            arrayItem.OwnerId = userId

            let item = await models[metaField.targetModel].upsert({ ...arrayItem }, { returning: true })
            await element[`add${key}`](item[0])
            return item[0]
          })
        } catch (e) {
          throw new Error(e)
        }

        relationsToRemove.forEach(async rel => {
          await rel.destroy()
        })
      }
    })

    await uploadImages(modelMeta, body)

    try {
      await element.update(body)
    } catch (e) {
      return res.status(500).send({ messages: [e.message] })
    }

    if (modelName === 'Model') {
      /*
      try {
        await generateModelFile(element, bodyToUpdate, false)
      } catch (e) {
        return res.status(500).send({ messages: [e.message] })
      }
      */
    }

    return res.status(200).send({ id: element.id })
  },

  async delete (req, res) {
    const user = await User.findByPk(req.userId, { include: [ models['Role'] ] })

    const id = req.params.id
    const modelName = req.get('Model')

    if (!modelName) {
      return res.status(400).send({ messages: ['Model not provided'] })
    }

    const hasPermission = await models.Model.findOne({
      where: {
        modelName
      },

      include: {
        model: models['ModelPermission'],
        required: !user.Role.hasFullAccess,
        where: {
          action: 'delete',
          RoleId: {
            [Op.eq]: user.Role.id,
          },
        },
      }
    })

    if (!hasPermission) {
      return res.status(403).send({ messages: ['Forbidden'] })
    }
    
    let result = null
    try {
      result = await models[modelName].findByPk(id)
    } catch (e) {
      console.error(e)
    }

    if (!result) {
      const model = await Model.findOne({ where: { modelName } })

      return res.status(404).send({ messages: [`${model.name} not found`] })
    }

    try {
      await result.destroy()
    } catch (e) {
      console.error(e)
      return res.status(500).send({ messages: ['Error destroying object'] })
    }

    return res.status(200).send()
  }
}

module.exports = magicController
