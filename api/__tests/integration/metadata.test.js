const test = require('ava');
const request = require('supertest');

const app = require('../../src/app');
const factory = require('../factories');

test('Should retrieve metadata not being authenticated in default language (en)', async t => {
  const response = await request(app)
    .get('/api/metadata')
    .send();

  t.is(response.status, 200);
  t.is(response.body.labels.name, 'Name')
});

test('Should retrieve metadata not being authenticated in pt_BR', async t => {
  const response = await request(app)
    .get('/api/metadata')
    .set('Language', 'pt_BR')
    .send();

  t.is(response.status, 200);
  t.is(response.body.labels.name, 'Nome')
});

test('Should retrieve metadata in default language (en) and after in pt_BR - verify multiple requests and languages', async t => {
  const langEn = await factory.create('Language', { name: 'English', abbr: 'en' })
  const langPt = await factory.create('Language', { name: 'Português', abbr: 'pt_BR' })

  const user1 = await factory.create('User', { password: '1234', LanguageId: langEn.id });
  const user2 = await factory.create('User', { password: '4321', LanguageId: langPt.id });

  const response1 = await request(app)
    .get('/api/metadata')
    .set('Authorization', `Bearer ${user1.generateToken()}`)
    .send();

  t.is(response1.body.labels.name, 'Name')

  const response2 = await request(app)
    .get('/api/metadata')
    .set('Authorization', `Bearer ${user2.generateToken()}`)
    .send();

  t.is(response2.body.labels.name, 'Nome')
});

test('Should return an error list when given language does not exist', async t => {
  const response = await request(app)
    .get('/api/metadata')
    .set('Language', 'es-AR')
    .send();

  t.is(response.status, 500)
  t.true(Array.isArray(response.body.messages))
})
