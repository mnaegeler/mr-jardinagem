export default function (data) {
  data = `${data}` // Ensure is string
  if (data.length === 11) {
    return data.replace(/(\d{3})(\d{3})(\d{3})(\d{2})/, '$1.$2.$3-$4')
  }

  if (data.length === 14) {
    return data.replace(/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/, '$1.$2.$3/$4-$5')
  }

  return data
}
