#!/bin/bash
mkdir -p mrdeploy;
tar -xzvf frontend.tar.gz -C mrdeploy/;
tar -xzvf backend.tar.gz -C mrdeploy/;
cd mrdeploy/api;
NODE_ENV=production node src/generators/syncDatabase.js;
NODE_ENV=production node src/generators/import.js -s;
source ~/.bash_profile;
pm2 restart all;
